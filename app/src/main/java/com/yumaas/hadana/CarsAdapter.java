package com.yumaas.hadana;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<CompanyModel> companyModels;


    public CarsAdapter(OnItemClickListener onItemClickListener, ArrayList<CompanyModel> companyModels) {
        this.onItemClickListener = onItemClickListener;
        this.companyModels = companyModels;

    }


    @Override
    public int getItemCount() {
        return companyModels == null ? 0 : companyModels.size();
    }


    @Override
    public CarsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_company, parent, false);
        CarsAdapter.ViewHolder viewHolder = new CarsAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final CarsAdapter.ViewHolder holder, final int position) {

        holder.imageView.setImageResource(companyModels.get(position).resource);
        holder.name.setText(companyModels.get(position).name);
        holder.distance.setText(companyModels.get(position).distance + " KM");
        holder.price.setText(companyModels.get(position).price + " EGP");

        holder.callphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "0123456789"));
                if (holder.itemView.getContext().checkSelfPermission(android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    Activity#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    return;
                }
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name, distance, price;
        Button callphone;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.image);
            name = view.findViewById(R.id.tv_review_reviewer);
            distance = view.findViewById(R.id.tv_review_distance);
            price = view.findViewById(R.id.tv_review_price);



        }
    }
}