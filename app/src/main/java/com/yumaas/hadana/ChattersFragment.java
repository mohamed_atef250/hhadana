package com.yumaas.hadana;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.Chat;
import com.yumaas.hadana.models.User;


import java.util.ArrayList;
import java.util.HashMap;


public class ChattersFragment extends Fragment {
    View rootView;
    RecyclerView menuList;
    HashMap<String,Boolean  >hashMap;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chatters, container, false);


        menuList = rootView.findViewById(R.id.menu_list);

        hashMap = new HashMap<>();

        ArrayList<Chat> chatters = new ArrayList<>();

        ArrayList<Chat> chattersTemp = DataBaseHelper.getDataLists().chats;
        User user = DataBaseHelper.getSavedUser();


        for(int i=0; i<chattersTemp.size(); i++){

            if(user.id == chattersTemp.get(i).user.id ||
                    (user.id == chattersTemp.get(i).hadana.id)){
                String text = Math.max(chattersTemp.get(i).user.id,chattersTemp.get(i).hadana.id)+""+
                Math.min(chattersTemp.get(i).hadana.id,chattersTemp.get(i).user.id);

                if(!hashMap.containsKey(text)){
                    chatters.add(chattersTemp.get(i));
                }

                hashMap.put(text,true);
            }

        }


        UserChattersAdapter chatAdapter = new UserChattersAdapter(getActivity(),chatters);
        menuList.setAdapter(chatAdapter);


        return rootView;
    }


}
