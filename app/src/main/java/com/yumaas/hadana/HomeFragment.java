package com.yumaas.hadana;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class HomeFragment extends Fragment implements OnMapReadyCallback {

    MapView mapView;
    LinearLayout homeList;
    GoogleMap map;
    ImageView changeImageView;
    FloatingActionButton fab;
    View v;
    boolean ok = true;
    boolean startLocation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_home, container, false);


        mapView = v.findViewById(R.id.mapview);
        homeList = v.findViewById(R.id.ll_home_list);
        changeImageView = v.findViewById(R.id.iv_change);
        fab = v.findViewById(R.id.fab);
        mapView.onCreate(savedInstanceState);


        mapView.getMapAsync(this);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLocation = true;
                Intent i = new Intent(getActivity(), SelectLocationActivity.class);
                i.putExtra("ok", true);
                startActivityForResult(i, 1);
            }
        });


        return v;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == getActivity().RESULT_OK) {

                if (startLocation) {

                    startLocation = false;
                    Intent i = new Intent(getActivity(), SelectLocationActivity.class);
                    i.putExtra("ok", false);
                    myLatLng = new LatLng(data.getDoubleExtra("lat", 0.0), data.getDoubleExtra("lng", 0.0));

                    startActivityForResult(i, 1);
                } else {

                    final ProgressDialog proDialog = ProgressDialog.show(getActivity(), "Getting Delivery", "Searching for companies or individuals deliveries");
                    proDialog.show();

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                Thread.sleep(4000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            proDialog.hide();
                            proDialog.cancel();
                            proDialog.dismiss();
                            getWorkers();
                        }
                    }.execute();

                }


            }


        }
    }

    boolean done = true;
    LatLng myLatLng, companyLatLng;
    double incresments[] = {.0432, .05231, .08532, .0496, .1144, .0409, .0330, .0910, .1312, .19032, .6089, .01421, .01124, .01312, .02312};

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setMyLocationButtonEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        map.setMyLocationEnabled(true);

        map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                if (done) {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
                    done = false;
                }
            }
        });

        changeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ok) {
                    mapView.setVisibility(View.GONE);
                    homeList.setVisibility(View.VISIBLE);
                } else {
                    homeList.setVisibility(View.GONE);
                    mapView.setVisibility(View.VISIBLE);
                }

                ok = !ok;
            }
        });


    }


    private Marker addMarker(int drawable, LatLng latLng, String title) {
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(drawable);

        MarkerOptions markerOptions = new MarkerOptions().position(latLng)
                .title(title)
                .snippet("Thanks for your selection")
                .icon(icon);

        return map.addMarker(markerOptions);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));


        return Radius * c;
    }


    private void getWorkers() {

            int counter = 0;
            map.setOnCameraIdleListener(null);
            ArrayList<CompanyModel> companyModels = new ArrayList<>();
            ArrayList<CompanyModel> companyModels2 = new ArrayList<>();


            Marker marker;

            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            for (int i = 0; i < 10; i++) {
                if (i > 6) {
                    incresments[i] = incresments[i] * -1;
                }
                if (i > 0 && i < 3) {
                    companyLatLng = new LatLng(myLatLng.latitude - incresments[i], myLatLng.longitude);
                } else if (i >= 3 && i <= 5) {
                    companyLatLng = new LatLng(myLatLng.latitude - incresments[i], myLatLng.longitude + incresments[i]);
                } else if (i >= 5 && i <= 8) {
                    companyLatLng = new LatLng(myLatLng.latitude + incresments[i], myLatLng.longitude + incresments[i]);
                } else {
                    companyLatLng = new LatLng(myLatLng.latitude + incresments[i], myLatLng.longitude - incresments[i]);
                }
                boolean isEven = ((i & 1) == 0);
                double dist = CalculationByDistance(myLatLng, companyLatLng);

                int userResources[] = {R.drawable.boy1, R.drawable.boy2, R.drawable.boy3, R.drawable.boy4, R.drawable.boy5};
                String names[] = {"Amr Mohamed", "Amal Ibrahim", "Yussra mohsen", "Khaled Rafat", "Ahmed khaled"};
                String companies[] = {"MSCO", "ECHO", "FedEx", "Uporisa", "Colorista"};
                int companyResources[] = {R.drawable.company1, R.drawable.company3, R.drawable.company4, R.drawable.company6, R.drawable.company7, R.drawable.company8};

                int resource = (isEven) ? R.drawable.ic_user_map : R.drawable.ic_company_map;
                String name = isEven ? names[(i % userResources.length)] : companies[counter % 5];
                String distance = "" + (int) dist;
                String price = " " + (int) (dist * (isEven ? 10 : 15));
                marker = addMarker(resource, companyLatLng, "User" + distance);

                resource = (isEven) ? userResources[i % userResources.length] : companyResources[counter];

                builder.include(marker.getPosition());


                if (!isEven) {
                    companyModels.add(new CompanyModel(resource, name, distance, price));
                } else {
                    companyModels2.add(new CompanyModel(resource, name, distance, price));
                    counter++;
                }

            }

            LatLngBounds bounds = builder.build();
            int padding = 0; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            map.animateCamera(cu);
            setList(companyModels2, companyModels);

            //map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));


    }

    private void setList(ArrayList<CompanyModel> companyModels, ArrayList<CompanyModel> companyModels2) {
        Bundle data = new Bundle();
        Bundle data2 = new Bundle();
        MovedData movedData = new MovedData(companyModels);
        MovedData movedData2 = new MovedData(companyModels2);
        data2.putSerializable("data", movedData2);

        data.putSerializable("data", movedData);
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getActivity().getSupportFragmentManager(), FragmentPagerItems.with(getActivity())
                .add("Users", CompaniesFragment.class, data)
                .add("Companies", CompaniesFragment.class, data2)
                .create());
        // CompaniesFragment companiesFragment = (CompaniesFragment) adapter.getItem(0);
        // companiesFragment.setData(companyModels);
        ViewPager viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) v.findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);
    }

}