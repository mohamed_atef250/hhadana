package com.yumaas.hadana.models;


import com.yumaas.hadana.base.DataBaseHelper;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
    public  int id,type,accepted;
   public String name,idNumber,email,phone,password,image,details,address,price;
    public ArrayList<Time>times ;
    public Time bookedTime ;
    public int bookedId = 0;

    public User(String name,String email,String phone,String password){
        this.id= DataBaseHelper.generateId();
        this.name=name;
        this.email=email;
        this.phone=phone;
        this.password=password;
        this.times= new ArrayList<>();
        type=0;accepted=0;

    }



}
