package com.yumaas.hadana.models;



import com.yumaas.hadana.base.DataBaseHelper;

import java.io.Serializable;
import java.util.ArrayList;

public class Ads implements Serializable {
    public int id;
    public int userId;
    public String title,details,image;
    public ArrayList<Comment>comments=new ArrayList<>();

    public Ads(String title,String details,String image){
        this.id= DataBaseHelper.generateId();
        this.title=title;
        this.details=details;
        this.image=image;
    }

}
