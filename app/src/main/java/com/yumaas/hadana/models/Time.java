package com.yumaas.hadana.models;

import java.io.Serializable;

public class Time implements Serializable {
   public String from,to;

   public Time(String from,String to){
       this.from=from;
       this.to=to;
   }
}
