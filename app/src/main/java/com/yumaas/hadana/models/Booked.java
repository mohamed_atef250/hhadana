package com.yumaas.hadana.models;



import com.yumaas.hadana.base.DataBaseHelper;

import java.io.Serializable;

public class Booked implements Serializable {

    public int id;
    public int userId,hadanaId;
    public Time time;

    public Booked(int userId , int hadanaId,Time time){
        this.id= DataBaseHelper.generateId();
        this.userId=userId;
        this.hadanaId=hadanaId;
        this.time=time;
    }
}
