package com.yumaas.hadana.models;

import java.io.Serializable;

public class Chat implements Serializable {

    public User user;
    public User hadana;
    public String message;
    public int sender;

    public Chat(User user, User hadana, String message){
        this.user = user;
        this.hadana = hadana;
        this.message=message;
    }

}
