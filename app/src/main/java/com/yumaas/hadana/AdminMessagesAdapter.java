package com.yumaas.hadana;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.models.UserMeessage;
import com.yumaas.hadana.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class AdminMessagesAdapter extends RecyclerView.Adapter<AdminMessagesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;

    ArrayList<UserMeessage>userMeessages;

    public AdminMessagesAdapter(OnItemClickListener onItemClickListener, ArrayList<UserMeessage>userMeessages) {
        this.onItemClickListener = onItemClickListener;
        this.userMeessages=userMeessages;
    }


    @Override
    public int getItemCount() {
        return userMeessages.size();
    }


    @Override
    public AdminMessagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);
        AdminMessagesAdapter.ViewHolder viewHolder = new AdminMessagesAdapter.ViewHolder(view);


        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final AdminMessagesAdapter.ViewHolder holder, final int position) {
        ConnectionHelper.loadImage(holder.image,userMeessages.get(position).user.image);
        holder.name.setText(userMeessages.get(position).user.name);
        holder.phone.setText(userMeessages.get(position).user.phone);
        holder.details.setText(userMeessages.get(position).comment);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name,phone,details;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            phone= view.findViewById(R.id.phone);
            details = view.findViewById(R.id.details);

        }
    }
}