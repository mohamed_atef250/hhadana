package com.yumaas.hadana;

import android.app.Activity;
import android.content.Context;
import android.view.View;


public class SweetDialogs {

    public static void successMessage(CustomDialogClass alertDialog, String message, View.OnClickListener onSweetClickListener) {


        alertDialog.show();
        alertDialog.setTitle("تمت بنجاح");
        alertDialog.setMessage(message);
        alertDialog.setButton("غلق", onSweetClickListener);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

    }

    public static void errorMessage(Context context, String message) {

        CustomDialogClass alertDialog = new CustomDialogClass((Activity) context);
        alertDialog.show();
        alertDialog.setTitle("خطا");
        alertDialog.setMessage(message);
        alertDialog.setButton("غلق", view -> {
            alertDialog.cancel();
            alertDialog.dismiss();
        });
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);


    }


}
