package com.yumaas.hadana;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.User;
import com.yumaas.hadana.volleyutils.ConnectionHelper;

import java.util.ArrayList;




public class AdminelegatesAdapter extends RecyclerView.Adapter<AdminelegatesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;

    ArrayList<User>users;

    public AdminelegatesAdapter(OnItemClickListener onItemClickListener,ArrayList<User>users) {
        this.onItemClickListener = onItemClickListener;
        this.users=users;
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public AdminelegatesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hadana, parent, false);
        AdminelegatesAdapter.ViewHolder viewHolder = new AdminelegatesAdapter.ViewHolder(view);


        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final AdminelegatesAdapter.ViewHolder holder, final int position) {
        ConnectionHelper.loadImage(holder.image,users.get(position).image);
        holder.name.setText(users.get(position).name);
        holder.phone.setText(users.get(position).phone);
        holder.details.setText(users.get(position).details);

        if(users.get(position).accepted==1){
            holder.accept.setVisibility(View.GONE);
            holder.reject.setText("مسح");
        }else {
            holder.accept.setVisibility(View.VISIBLE);
            holder.reject.setText("رفض");
        }


        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CustomDialogClass alertDialog = new CustomDialogClass((Activity) view.getContext());

                SweetDialogs.successMessage(alertDialog, "تم الموافقه بنجاح", view1 -> {
                    users.get(position).accepted=1;
                    User user  =  users.get(position);
                    alertDialog.cancel();
                    alertDialog.dismiss();
                    DataBaseHelper.updateUser(user);
                    notifyDataSetChanged();
                });
            }
        });

        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialogClass alertDialog = new CustomDialogClass((Activity) view.getContext());

                SweetDialogs.successMessage(alertDialog, "تم الرفض بنجاح", view1 -> {
                    User user  =  users.get(position);
                    alertDialog.cancel();
                    alertDialog.dismiss();
                    DataBaseHelper.removeUser(user);
                    users.remove(position);
                    notifyDataSetChanged();
                });


            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name,phone,details;
        Button reject,accept;
        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            phone = view.findViewById(R.id.phone);
            details = view.findViewById(R.id.details);
            reject=view.findViewById(R.id.reject);
            accept = view.findViewById(R.id.accept);
        }
    }
}