package com.yumaas.hadana.base;

import com.yumaas.hadana.models.Ads;
import com.yumaas.hadana.models.Book;
import com.yumaas.hadana.models.Booked;
import com.yumaas.hadana.models.Chat;
import com.yumaas.hadana.models.Order;
import com.yumaas.hadana.models.User;
import com.yumaas.hadana.models.UserMeessage;


import java.util.ArrayList;


public class DataLists {

    public ArrayList<User>  users = new ArrayList<>();
    public ArrayList<Book>  books = new ArrayList<>();
    public ArrayList<Booked>  bookeds = new ArrayList<>();
    public ArrayList<UserMeessage>userMeessages = new ArrayList<>();
    public ArrayList<Chat>  chats = new ArrayList<>();
    public ArrayList<Ads>   ads = new ArrayList<>();
    public ArrayList<Book>  flowers = new ArrayList<>();
    public ArrayList<Order> orders = new ArrayList<>();

}
