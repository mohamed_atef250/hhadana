package com.yumaas.hadana.teacher;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.OnItemClickListener;
import com.yumaas.hadana.R;

public class UsersChatFragments2 extends Fragment {


    View v;
    UsersChatAdapter2 citiesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_companies, container, false);

        RecyclerView recyclerView = v.findViewById(R.id.recyclerview);

        citiesAdapter = new UsersChatAdapter2(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        });
        recyclerView.setAdapter(citiesAdapter);
        return v;
    }





}