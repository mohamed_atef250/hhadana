package com.yumaas.hadana.teacher;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.OnItemClickListener;
import com.yumaas.hadana.R;


public class MyAdsAdapter extends RecyclerView.Adapter<MyAdsAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;



    public MyAdsAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;


    }


    @Override
    public int getItemCount() {
        return 10;
    }


    @Override
    public MyAdsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_ads, parent, false);
        MyAdsAdapter.ViewHolder viewHolder = new MyAdsAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final MyAdsAdapter.ViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.getContext().startActivity(new Intent(view.getContext(),MessengerActivity.class));
            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {



        public ViewHolder(View view) {
            super(view);



        }
    }
}