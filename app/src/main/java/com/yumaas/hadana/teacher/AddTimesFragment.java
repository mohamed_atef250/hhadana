package com.yumaas.hadana.teacher;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.yumaas.hadana.CustomDialogClass;
import com.yumaas.hadana.FragmentHelper;
import com.yumaas.hadana.R;
import com.yumaas.hadana.SweetDialogs;
import com.yumaas.hadana.TimesFragment;
import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.Time;
import com.yumaas.hadana.models.User;

import java.util.Calendar;


public class AddTimesFragment extends Fragment {


    View v;

    EditText from, to;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_add_time, container, false);


        from = v.findViewById(R.id.from);
        to = v.findViewById(R.id.to);

        selectTimeDialog(from);
        selectTimeDialog(to);


        v.findViewById(R.id.btn_login_sign_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                User user = DataBaseHelper.getSavedUser();

                user.times.add(new Time(from.getText().toString(), to.getText().toString()));


                DataBaseHelper.saveStudent(user);

                DataBaseHelper.updateUser(user);

                CustomDialogClass alertDialog = new CustomDialogClass(getActivity());

                SweetDialogs.successMessage(alertDialog,
                        "تم الاضافه بنجاح", view1 -> {
                            alertDialog.cancel();
                            alertDialog.dismiss();
                            FragmentHelper.popAllFragments(getActivity());
                            FragmentHelper.addFragment(getActivity(), new TimesFragment(), "TimesFragment");
                        });
            }
        });

        return v;
    }

    int mHour, mMinute;

    private void selectTimeDialog(EditText txtTime) {

        txtTime.setFocusable(false);
        txtTime.setClickable(true);

        txtTime.setOnClickListener((View.OnClickListener) view -> {

            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), (timePicker, hourOfDay, i1) -> {

                String text = ((hourOfDay % 12) < 10 ? "0" : "") + "" + (hourOfDay % 12) + ":00";
                if (hourOfDay >= 12) {
                    text += " ص ";
                } else {
                    text += " م ";
                }

                txtTime.setText(text);


            }, mHour, mMinute, false);
            timePickerDialog.show();
        });

    }


}