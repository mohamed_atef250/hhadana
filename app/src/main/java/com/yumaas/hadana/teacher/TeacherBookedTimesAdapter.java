package com.yumaas.hadana.teacher;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.CustomDialogClass;
import com.yumaas.hadana.OnItemClickListener;
import com.yumaas.hadana.R;
import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.Booked;
import com.yumaas.hadana.models.User;
import com.yumaas.hadana.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class TeacherBookedTimesAdapter extends RecyclerView.Adapter<TeacherBookedTimesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;

    public ArrayList<Booked> bookeds;


    public TeacherBookedTimesAdapter(OnItemClickListener onItemClickListener, ArrayList<Booked>bookeds ) {
        this.onItemClickListener = onItemClickListener;

        this.bookeds=bookeds;
    }


    @Override
    public int getItemCount() {
        return bookeds.size();
    }


    @Override
    public TeacherBookedTimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_teacher_booked_times, parent, false);
        TeacherBookedTimesAdapter.ViewHolder viewHolder = new TeacherBookedTimesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final TeacherBookedTimesAdapter.ViewHolder holder, final int position) {
            User hadana = DataBaseHelper.findUser(bookeds.get(position).userId);
        holder.time.setText(" من الساعه  "+bookeds.get(position).time.from+" الي الساعه "+bookeds.get(position).time.to);
        holder.name.setText(hadana.name);
        holder.phone.setText(hadana.phone);
        ConnectionHelper.loadImage(holder.image,hadana.image);

        holder.call.setOnClickListener(view -> {

            try {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + hadana.phone ));
                view.getContext().startActivity(intent);
            }catch (Exception e){
                e.getStackTrace();
            }
        });

        holder.cancel.setOnClickListener(view -> {
            CustomDialogClass
                    alertDialog = new CustomDialogClass((Activity) holder.itemView.getContext());
            alertDialog.show();
            alertDialog.setTitle("هل تريد الغاء الحجز");
            alertDialog.setMessage("بالتاكيد الغاء حجز هذا الموعد ؟");
            alertDialog.setButton("الغاء", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DataBaseHelper.removeBooked(bookeds.get(position));
                    alertDialog.cancel();
                    alertDialog.dismiss();
                    Toast.makeText(view.getContext(), "تم الغاء الحجز", Toast.LENGTH_SHORT).show();
                }
            });
            alertDialog.setButton2("غلق", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.cancel();
                    alertDialog.dismiss();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);

        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name,phone,time;
        Button cancel,call;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            phone = view.findViewById(R.id.phone);
            time = view.findViewById(R.id.time);
            cancel =  view.findViewById(R.id.cancel);
            call=  view.findViewById(R.id.call);
            image = view.findViewById(R.id.image);

        }
    }
}