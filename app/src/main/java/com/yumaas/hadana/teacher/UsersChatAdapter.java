package com.yumaas.hadana.teacher;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.OnItemClickListener;
import com.yumaas.hadana.R;


public class UsersChatAdapter extends RecyclerView.Adapter<UsersChatAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;


    public UsersChatAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    public int getItemCount() {
        return 10;
    }


    @Override
    public UsersChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_chat, parent, false);
        UsersChatAdapter.ViewHolder viewHolder = new UsersChatAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UsersChatAdapter.ViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.getContext().startActivity(new Intent(view.getContext(),MessengerActivity.class));
            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);
        }
    }
}