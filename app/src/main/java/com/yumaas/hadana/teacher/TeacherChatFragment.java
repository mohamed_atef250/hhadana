package com.yumaas.hadana.teacher;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.R;
import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.Chat;
import com.yumaas.hadana.models.User;

import java.util.ArrayList;


public class TeacherChatFragment extends Fragment {
    private View rootView;
    private RecyclerView menuList;
    ArrayList<Chat> chatters, chats;
    EditText nameEditText;
    Button chatBtn;
    User hadana,user;
    TeacherChatAdapter chatAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        menuList = rootView.findViewById(R.id.menu_list);

        nameEditText = rootView.findViewById(R.id.et_chat_message);
        chatBtn = rootView.findViewById(R.id.btn_chat);


        chats = new ArrayList<>();
        chatters = DataBaseHelper.getDataLists().chats;



        Chat chat = (Chat) getArguments().getSerializable("chatItem");
        user  = chat.user;
        hadana  = DataBaseHelper.getSavedUser();

        

        for (int i = 0; i < chatters.size(); i++) {

            if (chatters.get(i).hadana.id == hadana.id && chatters.get(i).user.id==user.id) {
                chats.add(chatters.get(i));
            }
        }
        
         chatAdapter = new TeacherChatAdapter(getActivity(), chats);
        menuList.setAdapter(chatAdapter);


        chatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Chat chat = new Chat(user,hadana,nameEditText.getText().toString());
                chat.sender=hadana.id;
                chats.add(chat);
                DataBaseHelper.addChat(chat);
                chatAdapter.notifyDataSetChanged();
                nameEditText.setText("");
            }
        });

        return rootView;
    }


}
