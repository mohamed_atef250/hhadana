package com.yumaas.hadana.teacher;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.OnItemClickListener;
import com.yumaas.hadana.R;


public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;



    public BookingAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;


    }


    @Override
    public int getItemCount() {
        return 10;
    }


    @Override
    public BookingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking, parent, false);
        BookingAdapter.ViewHolder viewHolder = new BookingAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final BookingAdapter.ViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.getContext().startActivity(new Intent(view.getContext(),MessengerActivity.class));
            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {



        public ViewHolder(View view) {
            super(view);



        }
    }
}