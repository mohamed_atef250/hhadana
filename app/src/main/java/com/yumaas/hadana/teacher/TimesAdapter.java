package com.yumaas.hadana.teacher;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.OnItemClickListener;
import com.yumaas.hadana.R;
import com.yumaas.hadana.models.Time;

import java.util.ArrayList;


public class TimesAdapter extends RecyclerView.Adapter<TimesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;

    public ArrayList<Time> times;

    public TimesAdapter(OnItemClickListener onItemClickListener,   ArrayList<Time>times ) {
        this.onItemClickListener = onItemClickListener;
        this.times=times;
    }


    @Override
    public int getItemCount() {
        return times.size();
    }


    @Override
    public TimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_teacher_time, parent, false);
        TimesAdapter.ViewHolder viewHolder = new TimesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final TimesAdapter.ViewHolder holder, final int position) {

        holder.name.setText(" من الساعه  "+times.get(position).from+" الي الساعه "+times.get(position).to);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
        }
    }
}