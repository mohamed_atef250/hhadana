package com.yumaas.hadana;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.models.Chat;
import com.yumaas.hadana.volleyutils.ConnectionHelper;


import java.util.ArrayList;


public class UserChattersAdapter extends RecyclerView.Adapter<UserChattersAdapter.ViewHolder> {

    Context context;
    ArrayList<Chat>chattersList;


    public UserChattersAdapter(Context context, ArrayList<Chat>chattersList) {
        this.context = context;
        this.chattersList=chattersList;
    }


    @Override
    public UserChattersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chatter, parent, false);
        UserChattersAdapter.ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UserChattersAdapter.ViewHolder holder, final int position) {

        holder.message.setText(chattersList.get(position).hadana.name);


       ConnectionHelper.loadImage(holder.image , chattersList.get(position).hadana.image);


        holder.itemView.setOnClickListener(view -> {

            UserChatFragment userChatFragment =   new UserChatFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("chatItem",chattersList.get(position));
            userChatFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(), userChatFragment, "ChatFragment");
        });

    }

    @Override
    public int getItemCount() {
        return chattersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.tv_review_reviewer);
            image = itemView.findViewById(R.id.image);
        }
    }
}
