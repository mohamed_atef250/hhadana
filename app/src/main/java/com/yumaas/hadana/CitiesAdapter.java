package com.yumaas.hadana;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;


public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;



    public CitiesAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;


    }


    @Override
    public int getItemCount() {
        return 10;
    }


    @Override
    public CitiesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false);
        CitiesAdapter.ViewHolder viewHolder = new CitiesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final CitiesAdapter.ViewHolder holder, final int position) {


    }


    static class ViewHolder extends RecyclerView.ViewHolder {



        public ViewHolder(View view) {
            super(view);



        }
    }
}