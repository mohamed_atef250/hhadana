package com.yumaas.hadana;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CompaniesFragment extends Fragment {

    ArrayList<CompanyModel> companyModels;
    View v;
    CarsAdapter carsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_companies, container, false);

        RecyclerView recyclerView = v.findViewById(R.id.recyclerview);
         carsAdapter = new CarsAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        },((MovedData) getArguments().getSerializable("data")).companyModels);
        recyclerView.setAdapter(carsAdapter);
        return v;
    }





}