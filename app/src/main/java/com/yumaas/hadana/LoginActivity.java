package com.yumaas.hadana;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.User;
import com.yumaas.hadana.teacher.TeacherMainActivity;
import com.yumaas.hadana.user.UserMainActivity;

public class


LoginActivity extends AppCompatActivity {
    EditText userName,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);

        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},342);


        userName = findViewById(R.id.et_login_email);
        password = findViewById(R.id.et_login_password);

        findViewById(R.id.tv_login_no_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage("من فضلك قم باختيار نوع الحساب التي تريد انشاءه")
                        .setCancelable(false)
                        .setPositiveButton("حاضنه", (dialog, id) -> {
                            startActivity(new Intent(LoginActivity.this, RegisterHadanaActivity.class));
                            finish();
                        })
                        .setNegativeButton("ام", (dialog, id) -> {
                            startActivity(new Intent(LoginActivity.this, RegisterHadanaActivity.class));
                            finish();
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });


        findViewById(R.id.tv_login_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage("من فضلك قم باختيار نوع الحساب التي تريد انشاءه")
                        .setCancelable(false)
                        .setPositiveButton("حاضنه", (dialog, id) -> {
                            startActivity(new Intent(LoginActivity.this, RegisterHadanaActivity.class));

                        })
                        .setNegativeButton("ام", (dialog, id) -> {
                            startActivity(new Intent(LoginActivity.this, RegisterActivity.class));

                        });
                AlertDialog alert = builder.create();
                alert.show();



            }
        });


        findViewById(R.id.btn_login_sign_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userName.getText().toString().contains("admin")) {
                    startActivity(new Intent(LoginActivity.this, AdminMainPage.class));
                } else {
                    User user = DataBaseHelper.loginUser(userName.getText().toString()
                            , password.getText().toString());
                    if (user != null) {
                        DataBaseHelper.saveStudent(user);
                        Intent intent = new Intent(LoginActivity.this, UserMainActivity.class);

                        if(user.type == 2 ){
                            intent = new Intent(LoginActivity.this, TeacherMainActivity.class);
                        }

                        startActivity(intent);


                    } else {
                        SweetDialogs.errorMessage(LoginActivity.this, "ناسف البريد الالكتروني او كلمه السر خطا");
                    }
                }
            }
        });
    }
}
