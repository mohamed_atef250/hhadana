package com.yumaas.hadana;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.Chat;
import com.yumaas.hadana.models.User;
import com.yumaas.hadana.volleyutils.ConnectionHelper;


import java.util.ArrayList;


public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private Context context;
    ArrayList<Chat> chattersList;

    User user;
    public ChatAdapter(Context context, ArrayList<Chat>chattersList) {
        this.context = context;
        this.chattersList = chattersList;
        user = DataBaseHelper.getSavedUser();
    }


    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        ChatAdapter.ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChatAdapter.ViewHolder holder, final int position) {



        if( (user.id==chattersList.get(position).sender)) {
            holder.chatterLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ConnectionHelper.loadImage(holder.image,chattersList.get(position).user.image);
        }else {
            holder.chatterLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            ConnectionHelper.loadImage(holder.image,chattersList.get(position).hadana.image);
        }


        holder.message.setText(chattersList.get(position).message);
    }

    @Override
    public int getItemCount() {
        return chattersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;
        public ViewHolder(View itemView) {
            super(itemView);

            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.message);
            image = itemView.findViewById(R.id.image);

        }
    }
}
