package com.yumaas.hadana;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.Ads;

import java.util.ArrayList;


public class FragmentHadanaAds extends Fragment {
    View rootView;
    RecyclerView menuList;
    int id;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_ads, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);

        ArrayList<Ads>ads =  DataBaseHelper.getDataLists().ads;
        ArrayList<Ads>adsTemp =  new ArrayList<>();
        id = DataBaseHelper.getSavedUser().id;



        for(int i=0; i<ads.size(); i++){
            if(ads.get(i).userId==id)
            adsTemp.add(ads.get(i));
        }

        rootView.findViewById(R.id.btn_login_sign_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper.addFragment(getActivity(),new FragmentAddAds(),"AddAdsFragment");
            }
        });

        AdsAdapter adsAdapter = new AdsAdapter(getActivity(),adsTemp);
        menuList.setAdapter(adsAdapter);



        return rootView;
    }


}
