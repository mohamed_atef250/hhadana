package com.yumaas.hadana.user;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.R;
import com.yumaas.hadana.ViewOperations;
import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.Booked;
import com.yumaas.hadana.models.User;

import java.util.ArrayList;


public class UserBookedTimes extends Fragment {
    private View rootView;
    private RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_times, container, false);

        menuList = rootView.findViewById(R.id.menu_list);
        ViewOperations.setRVHVertical(getActivity(),menuList);

        User user = DataBaseHelper.getSavedUser();

        ArrayList<Booked> bookeds =  DataBaseHelper.getDataLists().bookeds;
        ArrayList<Booked> bookedsTemp =  new ArrayList<>();

        for(int i=0; i<bookeds.size(); i++){

            if(bookeds.get(i).userId==user.id){

                bookedsTemp.add(bookeds.get(i));

            }
        }



        BookedTimesAdapter timesAdapter = new BookedTimesAdapter(position -> {

        }, bookedsTemp);
        menuList.setAdapter(timesAdapter);


        rootView.findViewById(R.id.btn_login_sign_in).setVisibility(View.GONE);

        return rootView;
    }


}
