package com.yumaas.hadana.user;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.CustomDialogClass;
import com.yumaas.hadana.OnItemClickListener;
import com.yumaas.hadana.R;
import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.Booked;
import com.yumaas.hadana.models.Time;
import com.yumaas.hadana.models.User;

import java.util.ArrayList;


public class UserTimesAdapter extends RecyclerView.Adapter<UserTimesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;

    public ArrayList<Time> times;
    public  int hadanaId;

    public UserTimesAdapter(OnItemClickListener onItemClickListener, ArrayList<Time>times,int hadanaId ) {
        this.onItemClickListener = onItemClickListener;
        this.hadanaId=hadanaId;
        this.times=times;
    }


    @Override
    public int getItemCount() {
        return times.size();
    }


    @Override
    public UserTimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_teacher_time, parent, false);
        UserTimesAdapter.ViewHolder viewHolder = new UserTimesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UserTimesAdapter.ViewHolder holder, final int position) {

        holder.name.setText(" من الساعه  "+times.get(position).from+" الي الساعه "+times.get(position).to);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialogClass
                        alertDialog = new CustomDialogClass((Activity) holder.itemView.getContext());
                alertDialog.show();
                alertDialog.setTitle("هل تريد الحجز");
                alertDialog.setMessage("بالتاكيد حجز هذا الموعد ؟");
                alertDialog.setButton("حجز", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        User user = DataBaseHelper.getSavedUser();
                        Booked booked = new Booked(user.id,hadanaId,times.get(position));
                        DataBaseHelper.addBooked(booked);
                        Toast.makeText(view.getContext(), "تم الحجز بنجاح", Toast.LENGTH_SHORT).show();
                        alertDialog.cancel();
                        alertDialog.dismiss();
                    }
                });
                alertDialog.setButton2("غلق", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.cancel();
                        alertDialog.dismiss();
                    }
                });
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);

            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
        }
    }
}