package com.yumaas.hadana.user;


import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.UserChatFragment;
import com.yumaas.hadana.FragmentHelper;
import com.yumaas.hadana.OnItemClickListener;
import com.yumaas.hadana.R;
import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.Chat;
import com.yumaas.hadana.models.User;
import com.yumaas.hadana.volleyutils.ConnectionHelper;

import java.util.ArrayList;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class UserHadanasAdapter extends RecyclerView.Adapter<UserHadanasAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;

    ArrayList<User>users;

    public UserHadanasAdapter(OnItemClickListener onItemClickListener, ArrayList<User>users) {
        this.onItemClickListener = onItemClickListener;
        this.users=users;
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public UserHadanasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_hadana, parent, false);
        UserHadanasAdapter.ViewHolder viewHolder = new UserHadanasAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UserHadanasAdapter.ViewHolder holder, final int position) {
        ConnectionHelper.loadImage(holder.image,users.get(position).image);
        holder.name.setText(users.get(position).name);
        holder.phone.setText(users.get(position).phone);
        holder.address.setText(users.get(position).address);

        holder.itemView.setOnClickListener(view -> {

            Bundle bundle = new Bundle();
            bundle.putInt("id",users.get(position).id);
            UserTimesFragment userTimesFragment = new UserTimesFragment();
            userTimesFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(),userTimesFragment,"userTimesFragment");

        });

        holder.rate.setOnClickListener(view -> {


            Dialog rankDialog = new Dialog(view.getContext());
            rankDialog.setContentView(R.layout.rank_dialog);
            rankDialog.setCancelable(true);
            RatingBar ratingBar = rankDialog.findViewById(R.id.dialog_ratingbar);
            ratingBar.setRating(0);
            Button updateButton =  rankDialog.findViewById(R.id.rank_dialog_button);
            updateButton.setOnClickListener(v -> {
                rankDialog.dismiss();
                Toast.makeText(v.getContext(), "تم التقيم بنجاح", Toast.LENGTH_SHORT).show();
            });
            rankDialog.show();


        });

        holder.chat.setOnClickListener(view -> {
            UserChatFragment userChatFragment =   new UserChatFragment();
            Chat chat = new Chat(DataBaseHelper.getSavedUser(),users.get(position),"");
            Bundle bundle = new Bundle();
            bundle.putSerializable("chatItem",chat);
            userChatFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(), userChatFragment, "ChatFragment");
        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name,phone,address;
        Button rate,chat;
        MaterialRatingBar ratingbar;
        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            phone= view.findViewById(R.id.phone);
            address= view.findViewById(R.id.address);
            rate = view.findViewById(R.id.rate);
            chat = view.findViewById(R.id.chat);
            ratingbar = view.findViewById(R.id.ratingbar);
        }
    }
}