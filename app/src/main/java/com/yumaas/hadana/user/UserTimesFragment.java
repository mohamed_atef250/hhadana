package com.yumaas.hadana.user;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.R;
import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.User;

import java.util.ArrayList;


public class UserTimesFragment extends Fragment {
    private View rootView;
    private RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_times, container, false);

        menuList = rootView.findViewById(R.id.menu_list);

        User user =null;
        int id=getArguments().getInt("id",0);
        ArrayList<User> users =  DataBaseHelper.getDataLists().users;

        for(int i=0; i<users.size(); i++){
            if(users.get(i).id==id){
                user=users.get(i);
            }
        }



        UserTimesAdapter timesAdapter = new UserTimesAdapter(position -> {

        }, user.times,id);
        menuList.setAdapter(timesAdapter);


        rootView.findViewById(R.id.btn_login_sign_in).setVisibility(View.GONE);

        return rootView;
    }


}
