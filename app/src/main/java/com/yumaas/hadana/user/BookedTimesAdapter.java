package com.yumaas.hadana.user;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.CustomDialogClass;
import com.yumaas.hadana.OnItemClickListener;
import com.yumaas.hadana.R;
import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.Booked;
import com.yumaas.hadana.models.User;
import com.yumaas.hadana.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class BookedTimesAdapter extends RecyclerView.Adapter<BookedTimesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;

    public ArrayList<Booked> bookeds;


    public BookedTimesAdapter(OnItemClickListener onItemClickListener, ArrayList<Booked>bookeds ) {
        this.onItemClickListener = onItemClickListener;

        this.bookeds=bookeds;
    }


    @Override
    public int getItemCount() {
        return bookeds.size();
    }


    @Override
    public BookedTimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booked_times, parent, false);
        BookedTimesAdapter.ViewHolder viewHolder = new BookedTimesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final BookedTimesAdapter.ViewHolder holder, final int position) {
            User hadana = DataBaseHelper.findUser(bookeds.get(position).hadanaId);
        holder.time.setText(" من الساعه  "+bookeds.get(position).time.from+" الي الساعه "+bookeds.get(position).time.to);
        holder.name.setText(hadana.name);
        holder.phone.setText(hadana.phone);
        ConnectionHelper.loadImage(holder.image,hadana.image);
        holder.cancel.setOnClickListener(view -> {
            CustomDialogClass
                    alertDialog = new CustomDialogClass((Activity) holder.itemView.getContext());
            alertDialog.show();
            alertDialog.setTitle("هل تريد الغاء الحجز");
            alertDialog.setMessage("بالتاكيد الغاء حجز هذا الموعد ؟");
            alertDialog.setButton("الغاء", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DataBaseHelper.removeBooked(bookeds.get(position));
                    alertDialog.cancel();
                    alertDialog.dismiss();
                    Toast.makeText(view.getContext(), "تم الغاء الحجز", Toast.LENGTH_SHORT).show();
                }
            });
            alertDialog.setButton2("غلق", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.cancel();
                    alertDialog.dismiss();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);

        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name,phone,time;
        Button cancel;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            phone = view.findViewById(R.id.phone);
            time = view.findViewById(R.id.time);
            cancel =  view.findViewById(R.id.cancel);
            image=view.findViewById(R.id.image);
        }
    }
}