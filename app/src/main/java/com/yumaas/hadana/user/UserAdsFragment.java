package com.yumaas.hadana.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import com.yumaas.hadana.R;
import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.Ads;


import java.util.ArrayList;

public class UserAdsFragment extends Fragment {
    View v;
    UserAdsAdapter citiesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_add_list, container, false);

        v.findViewById(R.id.card_register).setVisibility(View.GONE);
        RecyclerView recyclerView = v.findViewById(R.id.recyclerview);
        ArrayList<Ads> ads =  DataBaseHelper.getDataLists().ads;
         citiesAdapter = new UserAdsAdapter(getActivity(),ads);

        recyclerView.setAdapter(citiesAdapter);
        return v;
    }





}