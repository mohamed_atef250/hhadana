package com.yumaas.hadana.user;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.R;
import com.yumaas.hadana.models.Ads;
import com.yumaas.hadana.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class UserAdsAdapter extends RecyclerView.Adapter<UserAdsAdapter.ViewHolder> {


    Context context;
    ArrayList<Ads> adsArrayList;

    public UserAdsAdapter(Context context, ArrayList<Ads> adsArrayList) {
        this.context = context;
        this.adsArrayList = adsArrayList;
    }


    @Override
    public int getItemCount() {
        return adsArrayList.size();
    }


    @Override
    public UserAdsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ads, parent, false);
        UserAdsAdapter.ViewHolder viewHolder = new UserAdsAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UserAdsAdapter.ViewHolder holder, final int position) {
        holder.name.setText(adsArrayList.get(position).title);
        holder.details.setText(adsArrayList.get(position).details);
        holder.delete.setVisibility(View.GONE);
        ConnectionHelper.loadImage(holder.image, adsArrayList.get(position).image);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, details;
        ImageView image;
        Button delete;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.name);
            this.details = itemView.findViewById(R.id.details);
            this.image = itemView.findViewById(R.id.image);
            this.delete=itemView.findViewById(R.id.delete);
        }


    }
}