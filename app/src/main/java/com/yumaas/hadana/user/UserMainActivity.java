package com.yumaas.hadana.user;

import android.Manifest;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.yumaas.hadana.ChattersFragment;
import com.yumaas.hadana.FragmentAddMessage;
import com.yumaas.hadana.FragmentHelper;
import com.yumaas.hadana.R;

public class UserMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 134);
        }
        
        setBottomBar();
        
        FragmentHelper.addFragment(this, new UserHadanasFragment(), "UserHadanasFragment");
    }


    private void setBottomBar(){
        AHBottomNavigation bottomNavigation =  findViewById(R.id.bottom_navigation);

        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.menu_home, R.drawable.ic_home, R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.ads, R.drawable.ic_ads, R.color.colorPrimary);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.booking, R.drawable.ic_booking, R.color.colorPrimary);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.messages, R.drawable.ic_messages, R.color.colorPrimary);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.problems, R.drawable.ic_messages, R.color.colorPrimary);

        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (position==0) {
                    FragmentHelper.addFragment(UserMainActivity.this, new UserHadanasFragment(), "UserAdsFragment");
                }
                else if(position==1) {
                    FragmentHelper.addFragment(UserMainActivity.this, new UserAdsFragment(), "UserHadanasFragment");
                }else if (position==2) {
                    FragmentHelper.addFragment(UserMainActivity.this, new UserBookedTimes(), "UserBookedTimes");
                }else if (position==4) {
                    FragmentHelper.addFragment(UserMainActivity.this, new FragmentAddMessage(), "FragmentAddMessage");
                }
                else {
                    FragmentHelper.addFragment(UserMainActivity.this, new ChattersFragment(), "ChattersFragment");

                }
                return true;
            }
        });

        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));
        bottomNavigation.setTranslucentNavigationEnabled(true);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


}