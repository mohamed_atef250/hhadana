package com.yumaas.hadana;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.Ads;
import com.yumaas.hadana.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.ViewHolder> {

    Context context;
    ArrayList<Ads> adsArrayList;

    public AdsAdapter(Context context, ArrayList<Ads> adsArrayList) {
        this.context = context;
        this.adsArrayList = adsArrayList;
    }


    @Override
    public AdsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_ads, parent, false);
        AdsAdapter.ViewHolder viewHolder = new AdsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdsAdapter.ViewHolder holder, final int position) {
        holder.name.setText(adsArrayList.get(position).title);
        holder.details.setText(adsArrayList.get(position).details);
        ConnectionHelper.loadImage(holder.image, adsArrayList.get(position).image);


        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialogClass
                        alertDialog = new CustomDialogClass((Activity) holder.itemView.getContext());
                alertDialog.show();
                alertDialog.setTitle("هل تريد المسح");
                alertDialog.setMessage("بالتاكيد مسح هذا الاعلان ؟");
                alertDialog.setButton("حذف", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DataBaseHelper.removeAd(adsArrayList.get(position));
                        adsArrayList.remove(position);
                        notifyDataSetChanged();
                        alertDialog.cancel();
                        alertDialog.dismiss();
                    }
                });
                alertDialog.setButton2("غلق", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.cancel();
                        alertDialog.dismiss();
                    }
                });
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
            }
        });




    }

    @Override
    public int getItemCount() {
        return adsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, details;
        ImageView image;
        Button delete;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.name);
            this.details = itemView.findViewById(R.id.details);
            this.image = itemView.findViewById(R.id.image);
            this.delete = itemView.findViewById(R.id.delete);
        }
    }
}
