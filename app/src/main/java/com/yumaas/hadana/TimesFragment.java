package com.yumaas.hadana;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.teacher.AddTimesFragment;
import com.yumaas.hadana.teacher.TimesAdapter;


public class TimesFragment extends Fragment {
    private View rootView;
    private RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_times, container, false);

        menuList = rootView.findViewById(R.id.menu_list);


         TimesAdapter timesAdapter = new TimesAdapter(new OnItemClickListener() {
             @Override
             public void onItemClickListener(int position) {

             }
         }, DataBaseHelper.getSavedUser().times);
        menuList.setAdapter(timesAdapter);


        rootView.findViewById(R.id.btn_login_sign_in).setOnClickListener(view -> FragmentHelper.addFragment(getActivity(),new AddTimesFragment(),"AddTimesFragment"));

        return rootView;
    }


}
