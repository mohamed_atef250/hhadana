package com.yumaas.hadana;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.User;
import com.yumaas.hadana.models.UserMeessage;


public class FragmentAddMessage extends Fragment {
    View rootView;
    EditText  details;

    User user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_message, container, false);


        details = rootView.findViewById(R.id.details);


        user = DataBaseHelper.getSavedUser();


        rootView.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DataBaseHelper.addMessage(new UserMeessage(details.getText().toString(),DataBaseHelper.getSavedUser()));
                Toast.makeText(getActivity(), "تمت الارسال بنجاح", Toast.LENGTH_SHORT).show();
                details.setText("");
            }
        });


        return rootView;
    }



}
