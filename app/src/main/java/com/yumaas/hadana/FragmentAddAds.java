package com.yumaas.hadana;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.base.Validate;
import com.yumaas.hadana.base.filesutils.FileOperations;
import com.yumaas.hadana.base.filesutils.VolleyFileObject;
import com.yumaas.hadana.models.Ads;
import com.yumaas.hadana.models.User;
import com.yumaas.hadana.volleyutils.ConnectionHelper;
import com.yumaas.hadana.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;


public class FragmentAddAds extends Fragment {
    View rootView;
    EditText name, details, image;

    boolean isEdit = false;
    public Ads ads;
    User user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_ads, container, false);

        name = rootView.findViewById(R.id.name);
        details = rootView.findViewById(R.id.details);
        image = rootView.findViewById(R.id.image);
        Button addd = rootView.findViewById(R.id.btn);

        user = DataBaseHelper.getSavedUser();


        image.setFocusable(false);
        image.setClickable(true);


        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
            }
        });


        try {
            ads = (Ads) getArguments().getSerializable("ad");
            name.setText(ads.title);
            details.setText(ads.details);
            selectedImage = ads.image;
            ads.userId = user.id;
            image.setText("تغير الصوره");
            if (ads != null) {
                isEdit = true;
                addd.setText("تعديل");
            }

        } catch (Exception e) {
            e.getStackTrace();
        }


        rootView.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {


                    if (isEdit) {
                        Ads center = new Ads(
                                name.getText().toString(),
                                details.getText().toString(),
                                selectedImage);
                        center.id = FragmentAddAds.this.ads.id;
                        ads.userId = user.id;
                        DataBaseHelper.updateAds(center);
                        CustomDialogClass alertDialog = new CustomDialogClass(getActivity());


                        SweetDialogs.successMessage(alertDialog, "تم التعديل بنجاح", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.cancel();
                                alertDialog.dismiss();
                                FragmentHelper.popAllFragments(getActivity());
                                FragmentHelper.addFragment(getActivity(), new FragmentHadanaAds(), "FragmentHadanaAds");

                            }
                        });

                    } else {
                        Ads center = new Ads(name.getText().toString(), details.getText().toString(), selectedImage);
                        center.userId = user.id;
                        DataBaseHelper.addAds(center);
                        CustomDialogClass alertDialog = new CustomDialogClass(getActivity());


                        SweetDialogs.successMessage(alertDialog, "تم الاضافه بنجاح", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.cancel();
                                alertDialog.dismiss();
                                FragmentHelper.popAllFragments(getActivity());
                                FragmentHelper.addFragment(getActivity(), new FragmentAddAds(), "FragmentAddAds");

                            }
                        });
                    }


                }
            }
        });


        return rootView;
    }

    private boolean validate() {
        if (Validate.isEmpty(name.getText().toString())) {
            name.setError("من فضلك املا الحقل");
            return false;
        }

        if (Validate.isEmpty(details.getText().toString())) {
            details.setError("من فضلك املا الحقل");
            return false;
        }
        return true;
    }


    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        volleyFileObjects = new ArrayList<>();
        VolleyFileObject volleyFileObject =
                FileOperations.getVolleyFileObject(getActivity(), data, "image",
                        43);


        image.setText("تم اضافه الهويه بنجاح");

        volleyFileObjects.add(volleyFileObject);


        addServiceApi();


    }


    String selectedImage = "";

    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                Log.e("DAta", "" + selectedImage);

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }


}
