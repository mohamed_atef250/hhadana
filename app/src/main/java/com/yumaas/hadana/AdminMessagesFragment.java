package com.yumaas.hadana;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.base.DataBaseHelper;

public class AdminMessagesFragment extends Fragment {


    View v;
    AdminMessagesAdapter citiesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_add_list, container, false);

        RecyclerView recyclerView = v.findViewById(R.id.recyclerview);
        v.findViewById(R.id.register).setVisibility(View.GONE);

        citiesAdapter = new AdminMessagesAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, DataBaseHelper.getDataLists().userMeessages);
        recyclerView.setAdapter(citiesAdapter);
        return v;
    }





}