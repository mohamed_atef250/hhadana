package com.yumaas.hadana;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;


public class DelegateAdapter extends RecyclerView.Adapter<DelegateAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;



    public DelegateAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;


    }


    @Override
    public int getItemCount() {
        return 10;
    }


    @Override
    public DelegateAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hadana, parent, false);
        DelegateAdapter.ViewHolder viewHolder = new DelegateAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final DelegateAdapter.ViewHolder holder, final int position) {


    }


    static class ViewHolder extends RecyclerView.ViewHolder {



        public ViewHolder(View view) {
            super(view);



        }
    }
}