package com.yumaas.hadana;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.hadana.base.DataBaseHelper;
import com.yumaas.hadana.models.User;

import java.util.ArrayList;

public class AdminHadanasFragment extends Fragment {


    View v;
    AdminelegatesAdapter citiesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_companies, container, false);

        RecyclerView recyclerView = v.findViewById(R.id.recyclerview);

        ArrayList<User>users = new ArrayList<>();
        ArrayList<User>usersTemp = DataBaseHelper.getDataLists().users;

        for(int i=0; i<usersTemp.size(); i++){
            if(usersTemp.get(i).type==2)
            users.add(usersTemp.get(i));
        }

        citiesAdapter = new AdminelegatesAdapter(position -> {

        },users);
        recyclerView.setAdapter(citiesAdapter);
        return v;
    }





}