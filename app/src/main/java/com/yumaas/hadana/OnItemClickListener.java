package com.yumaas.hadana;

public interface OnItemClickListener {
    public void onItemClickListener(int position);
}
